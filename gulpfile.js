const gulp          = require('gulp'),
      stylus        = require('gulp-stylus'),
      autoprefixer  = require('gulp-autoprefixer'),
      browserSync   = require('browser-sync').create();

gulp.task('styles', function() {
  return gulp.src('src/stylus/**/*.styl')
    .pipe(stylus())
    .pipe(autoprefixer(['last 2 versions', '> 1%', 'ie 11'], { cascade: true }))  // добавляем вендорные префиксы
    .pipe(gulp.dest('src/css'))
    .pipe(browserSync.reload({stream: true}))  // сохраняем курсор на старом месте после reload страницы
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: 'src'
    },
    notify: false
  });
  browserSync.watch('src', browserSync.reload)
});

gulp.task('watch', function(){
  gulp.watch('src/stylus/**/*.styl', gulp.series('styles'))
});

gulp.task('default', gulp.series(
  // gulp.series('styles'),
  gulp.parallel('watch', 'browser-sync')
))